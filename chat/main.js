 const endpoint = "http://localhost:8080/api/";

 const messgesEndpoint = endpoint + "chat/message";

 const clearAction = "clear";

 const updateAction = "update";

 const doneAction = "done";


let first = true;

class Message {
    
    constructor(id, name, text) {
        this.id = id;
        this.name = name;
        this.text = text;
    }

    createDomMsg(dom) {
        return dom.createMessageElement(this.id, this.name, this.text);
    }

    commapre(id, name, text) {
        return this.name === name && this.text == text;
    }

    update(id, name, text) {
        this.id = id;
        this.name = name;
        this.text = text;
    }
}

class Chat {

    constructor(){
        this.http = new HttpIo();
        this.render = new Render();
        this.dom = new Dom();
        this.messages = [];
        this.events = new Listners(this.dom, this.http, this.deleteMessage.bind(this));
    }


    init() {
        first = false;
        this.http.getData().then(data => {
            this.createMessagesList(data);
            this.addNewMsg(this.messages).then(() => this.scroll(this.dom.getChatRef()));
        })
    }

    update() {
        this.http.getData().then(data => {
            data.reverse().some((msg, key) => {
                
                if (!this.messages[key]) {
                    const newMessages = data.slice(key).map(val => new Message(val.id, val.username, val.text));
                    this.addNewMsg(newMessages).then(() => this.scroll(this.dom.getChatRef()));
                    this.messages = this.messages.concat(newMessages);
                    return;
                }

                if (msg.id === this.messages[key].id) {
                    
                    if (!this.messages[key].commapre(msg.id, msg.username, msg.text)) {
                        this.messages[key].update( msg.id, msg.username, msg.text);
                        this.dom.updateMessage(this.dom.getMsgById(this.messages[key].id), this.messages[key]);
                    }
                } else {
                    this.deleteMessage(this.messages[key].id);
                }                
            });
            
            if (data.length < this.messages.length) {
                this.messages.slice(data.length).forEach(msg => this.dom.deleteMsgFromDom(msg.id));
                this.messages.length = data.length;
            }
        });
    }

    createMessagesList(data) {
        this.messages = data
        .map(val => new Message(val.id, val.username, val.text)).reverse();
    }

    addNewMsg(newMessages) {
        if (newMessages.length) {
            return this.render.renderChat(
                this.dom.getChatRef(),
                newMessages.map(msg => msg.createDomMsg(this.dom))
            );
        }
    }

    deleteMessage(id) {
        this.messages.splice(this.messages.findIndex(val => val.id == id), 1);
        this.dom.deleteMsgFromDom(id);
    }

    scroll(chatRef){
        chatRef.children[this.messages.length - 1].scrollIntoView({
            behavior: "smooth",
            block: "end"
        });
    }
}

class Render {

    renderChat(chatRef, messages) {
        let i = 0;
        return new Promise(resolve => {
            const interval = setInterval(() => {
                chatRef.appendChild(messages[i]);
                i++;
                if(i > messages.length - 1) {
                    clearInterval(interval);
                    resolve();
                }
            }, 70);
        });
    }
}

class Dom {

    getChatRef() {
        return document.querySelector('.chat__messagess');
    }

    createMessageElement(id, name, text) {
        const template = document.getElementById('message');
        template.content.querySelector('.chat__message-username').textContent = name;
        template.content.querySelector('.chat__message-test').textContent = text;
        template.content.querySelector('.chat__message').setAttribute('id', id);
        template.content.querySelector('.chat__message').classList.add('animated', 'fadeInLeft');
        return template.content.cloneNode(true);
    }

    getMsgById(id) {
        return document.getElementById(id);
    }

    updateMessage(msgRef, msg) {
        msgRef.querySelector('.chat__message-username').innerHTML = '';
        msgRef.querySelector('.chat__message-test').innerHTML = '';
        msgRef.querySelector('.chat__message-username').textContent = msg.name;
        msgRef.querySelector('.chat__message-test').textContent = msg.text;
        msgRef.classList.remove('fadeInLeft');
        msgRef.setAttribute('id', msg.id);
        msgRef.classList.add('pulse');
        setTimeout(() => msgRef.classList.remove('pulse'), 1000);
    }

    getSubjectBtnRef() {
        return document.getElementById("send-btn");
    }

    getUsersnameFromNewMessage() {
        return document.getElementById("name").value;
    }

    getTextFromNewMessage() {
        return document.getElementById("text").value;
    }

    clearName() {
        document.getElementById("name").value = '';
    }

    clearText() {
        document.getElementById("text").value = '';
    }

    deleteMsgFromDom(id) {
        const elm = this.getMsgById(id);
        elm.classList.add('fadeOutRight');
        setTimeout(() => elm.remove(), 1000);
    }

    editMessage(id) {
        const elm = this.getMsgById(id);
        const nameElm = elm.querySelector('.chat__message-username');
        const textElm = elm.querySelector('.chat__message-test');
        const name = elm.querySelector('.chat__message-username').textContent;
        const text = elm.querySelector('.chat__message-test').textContent;
        nameElm.innerHTML = '';
        textElm.innerHTML = '';
        nameElm.appendChild(this.createInput(name));
        textElm.appendChild(this.createTextarea(text));
        elm.querySelector('.chat__message-create').classList.add("_hide");
        elm.querySelector(".chat__message-done").classList.add("_visibility");
    }

    createInput(val) {
        const elm = document.createElement("input");
        elm.classList.add('chat__form-text-input');
        elm.setAttribute('placeholder', 'не может быть пустым');
        if (val) {
            elm.value = val;
        }
        return elm;
    }

    createTextarea(val) {
        const elm = document.createElement("textarea");
        elm.classList.add('chat__form-text-textbox');
        elm.setAttribute('placeholder', 'не может быть пустым');
        if (val) {
            elm.value = val;
        }
        return elm;
    }

    getTextFromMessageByIdAndSelector(id, selector) {
        const value = this.getMsgById(id).querySelector(selector).value;
        const text = this.getMsgById(id).querySelector(selector).textContent;

        return value || text;
    }
}

class Listners {

    constructor(dom, http, deleteMsgCallback){
        dom.getSubjectBtnRef().addEventListener('click', this.addMsgHeandler.bind(this, dom, http));
        dom.getChatRef().addEventListener('click', this.messgesClickHeandler.bind(this, dom, http, deleteMsgCallback));
    }

    addMsgHeandler(dom, http, event) {

        dom.getSubjectBtnRef().classList.add('_stop-event');

        http.sendMsg({
            username: dom.getUsersnameFromNewMessage(),
            text: dom.getTextFromNewMessage()
        }).then(() => {
            dom.clearName();
            dom.clearText();
            dom.getSubjectBtnRef().classList.remove('_stop-event');
        });
    }

    messgesClickHeandler(dom, http, deleteMsgCallback, event) {

        if(event.target.dataset.action && event.target.dataset.action === clearAction) {
            http.deleteMsg(event.target.parentElement.getAttribute('id'))
            .then(resp => {
                if (resp.status === 200) {
                    dom.deleteMsgFromDom(event.target.parentElement.getAttribute('id'));
                    deleteMsgCallback(event.target.parentElement.getAttribute('id'));
                }
            });
        }

        if(event.target.dataset.action && event.target.dataset.action === updateAction) {
            dom.editMessage(event.target.parentElement.getAttribute('id'));
        }

        if(event.target.dataset.action && event.target.dataset.action === doneAction) {
            const id = event.target.parentElement.getAttribute('id');
            const name = dom.getTextFromMessageByIdAndSelector(id, ".chat__form-text-input");
            const text = dom.getTextFromMessageByIdAndSelector(id, ".chat__form-text-textbox");
    
            if(name && text) {
                http.editMsg(id, {
                    username: name,
                    text
                }).then( resp => {
                    if(resp.status === 200) {
                        event.target.parentElement.querySelector('.chat__message-done').classList.remove("_visibility");
                        event.target.parentElement.querySelector(".chat__message-create").classList.remove("_hide");
                        dom.updateMessage(event.target.parentElement, {id, name, text});
                    }
                });
            };
        }
    }
}

class HttpIo {

    getData() {
        return fetch(messgesEndpoint).then(data => data.json());
    }

    sendMsg(msg) {
        return fetch(messgesEndpoint, {
            method: "POST",
            body: JSON.stringify(msg),
            headers: {
                "Content-Type": "application/json"
            },
        });
    }

    deleteMsg(id) {
        return fetch(messgesEndpoint + "/" + id, {
            method: "DELETE"
        });
    }

    editMsg(id, msg) {
        return fetch(messgesEndpoint + "/" + id, {
            method: "PUT",
            body: JSON.stringify(msg),
            headers: {
                "Content-Type": "application/json"
            },
        });
    }
}

window.onload = () => {
    const chat = new Chat();
    chat.init();
    setInterval(() => {
        chat.update();
    }, 1000);
}